﻿using EncryptionLibrary.Keys;
using System.IO;
using System.Security.Cryptography;

namespace EncryptionLibrary
{
    public class EncryptionFactory
    {
        public int IVLength { get; }
        public int KeyLength { get; }

        RNGCryptoServiceProvider RNG { get; set; }
        IKeyFactory KeyFactory { get; set; }

        public EncryptionFactory(IKeyFactory KeyFactory, int IVLength = 16, int KeyLength = 32)
        {
            RNG = new RNGCryptoServiceProvider();
            this.KeyFactory = KeyFactory;
            this.IVLength = IVLength;
            this.KeyLength = KeyLength;
        }

        public void Encrypt(string Password, string InputFilePath)
        {
            Encrypt(Password, InputFilePath, InputFilePath + ".enc");
        }

        public void Encrypt(string Password, string inputFilePath, string outputFilePath)
        {
            KeyFactory.DeriveKey(Password);
            using (var aes = Aes.Create())
            {
                aes.Key = KeyFactory.Key;
                aes.IV = GetIV(IVLength);
                using (var input = File.OpenRead(inputFilePath))
                {
                    using (var output = File.Create(outputFilePath))
                    {
                        output.Write(aes.IV, 0, aes.IV.Length);
                        KeyFactory.WriteParameters(output);
                        using (var encryptor = aes.CreateEncryptor())
                        {
                            using (var cryptoWriter = new CryptoStream(output, encryptor, CryptoStreamMode.Write))
                            {
                                input.CopyTo(cryptoWriter);
                            }
                        }
                    }
                }
            }
        }

        private byte[] GetIV(int length = 16)
        {
            var bytes = new byte[length];
            RNG.GetBytes(bytes);
            return bytes;
        }
    }
}
