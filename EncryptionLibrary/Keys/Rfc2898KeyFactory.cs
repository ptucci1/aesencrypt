﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace EncryptionLibrary.Keys
{
    /// <summary>
    /// An implementation of IKeyFactory that implements the RFC2898 key generation protocol.
    /// </summary>
    public class Rfc2898KeyFactory : IKeyFactory
    {
        public byte[] Key { get; set; }
        public byte[] Salt { get; set; }
        public int Iterations { get; private set; }

        RNGCryptoServiceProvider RNG { get; }

        public Rfc2898KeyFactory(int KeyLength, int SaltLength, int Iterations = 1000)
        {
            Key = new byte[KeyLength];
            Salt = new byte[SaltLength];
            this.Iterations = Iterations;
            RNG = new RNGCryptoServiceProvider();
        }

        public void DeriveKey(string Password)
        {
            RNG.GetBytes(Salt);
            ComputeKey(Password, Salt, Iterations);
        }

        public void DeriveKey(string Password, FileStream encryptedFileStream)
        {
            var iterationBytes = new byte[4];
            encryptedFileStream.Read(iterationBytes, 0, iterationBytes.Length);
            Iterations = BitConverter.ToInt32(iterationBytes, 0);
            encryptedFileStream.Read(Salt, 0, Salt.Length);
            ComputeKey(Password, Salt, Iterations);
        }

        private void ComputeKey(string Password, byte[] Salt, int Iterations)
        {
            var key = new Rfc2898DeriveBytes(Password, Salt, Iterations);
            Key = key.GetBytes(Key.Length);
        }

        public void WriteParameters(FileStream outputFileStream)
        {
            var iterations = BitConverter.GetBytes(Iterations);
            outputFileStream.Write(iterations, 0, iterations.Length);
            outputFileStream.Write(Salt, 0, Salt.Length);
        }
    }
}
