﻿using System.IO;

namespace EncryptionLibrary.Keys
{
    /// <summary>
    /// Defines functionality for generating encryption keys.
    /// </summary>
    public interface IKeyFactory
    {
        /// <summary>
        /// The derived key bytes.
        /// </summary>
        byte[] Key { get; }

        /// <summary>
        /// Derives a key. Uses new random values for any implementation-specific parameters, like salts.
        /// </summary>
        /// <param name="Password">The password to use to derive the key.</param>
        void DeriveKey(string Password);

        /// <summary>
        /// Derives a key. Accepts a FileStream from which implementation-specific parameters, like salts, can be read.
        /// </summary>
        /// <param name="Password">The password to use to derive the key.</param>
        /// <param name="encryptedFileStream">The source FileStream for implementation-specific parameters.</param>
        void DeriveKey(string Password, FileStream encryptedFileStream);

        /// <summary>
        /// Writes implementation-specific parameters to the output file.
        /// </summary>
        /// <param name="outputFileStream">A FileStream for the output file. This should NOT be the encrypted file stream. The parameters must be written in plain-text so that they can be read when decrypting.</param>
        void WriteParameters(FileStream outputFileStream);
    }
}
