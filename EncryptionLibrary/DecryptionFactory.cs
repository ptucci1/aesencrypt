﻿using EncryptionLibrary.Keys;
using System.IO;
using System.Security.Cryptography;

namespace EncryptionLibrary
{
    public class DecryptionFactory
    {
        IKeyFactory KeyFactory { get; set; }
        public int IVLength { get; }
        public int KeyLength { get; }

        public DecryptionFactory(IKeyFactory KeyFactory, int IVLength = 16, int KeyLength = 32)
        {
            this.KeyFactory = KeyFactory;
            this.IVLength = IVLength;
            this.KeyLength = KeyLength;
        }

        public void Decrypt(string Password, string inputFilePath)
        {
            var outputPath = GetOutputFilePath(inputFilePath);
            Decrypt(Password, inputFilePath, outputPath);
        }

        public void Decrypt(string Password, string inputFilePath, string outputFilePath)
        {
            using (var encryptedReader = new FileStream(inputFilePath, FileMode.Open, FileAccess.Read))
            {
                using (var aes = Aes.Create())
                {
                    var iv = new byte[IVLength];
                    encryptedReader.Read(iv, 0, iv.Length);
                    KeyFactory.DeriveKey(Password, encryptedReader);
                    aes.IV = iv;
                    aes.Key = KeyFactory.Key;
                    using (var decryptedWriter = new FileStream(outputFilePath, FileMode.Create, FileAccess.Write))
                    {
                        using (var decryptor = aes.CreateDecryptor())
                        {
                            using (var cryptoStream = new CryptoStream(encryptedReader, decryptor, CryptoStreamMode.Read))
                            {
                                cryptoStream.CopyTo(decryptedWriter);
                            }
                        }
                    }
                }
            }
        }

        private string GetOutputFilePath(string inputFilePath)
        {
            var file = new FileInfo(inputFilePath);
            file = new FileInfo(file.FullName.Replace(file.Extension, ""));
            return file.FullName.Replace(file.Extension, $"_decrypted{file.Extension}");
        }
    }
}
