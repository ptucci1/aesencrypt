﻿using GUI.Views.Main.Views;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Text.Json;
using System.Windows.Forms;

namespace GUI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var services = GetServices();
            Application.EnableVisualStyles();
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainView(services.GetService<Configuration>()));
        }

        static IServiceProvider GetServices()
        {
            var config = JsonSerializer.Deserialize<Configuration>(File.ReadAllText("config.json"));
            var services = new ServiceCollection();

            services.AddSingleton(typeof(Configuration), (sp) => config);

            return services.BuildServiceProvider();
        }
    }
}
