﻿using EncryptionLibrary;
using EncryptionLibrary.Keys;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI.Views.Main.Views
{
    public partial class MainView : Form
    {
        public Configuration Configuration { get; }

        public MainView(Configuration Configuration)
        {
            InitializeComponent();
            this.Configuration = Configuration;
        }

        private void MainView_Load(object sender, EventArgs e)
        {
            OperationCmb.SelectedIndex = 0;
        }

        private void ChooseFileBtn_Click(object sender, EventArgs e)
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.Title = "Choose encrypted file:";
                ofd.Multiselect = false;
                ofd.Filter = "Encrypted Files (*.enc)|*.enc|All Files|*.*";
                ofd.AddExtension = true;
                if(ofd.ShowDialog() == DialogResult.OK)
                {
                    FileTxt.Text = ofd.FileName;
                }
            }
        }

        private async void StartBtn_Click(object sender, EventArgs e)
        {
            var password = PasswordTxt.Text;
            if(OperationCmb.SelectedItem.ToString().IndexOf("encrypt", StringComparison.CurrentCultureIgnoreCase) >= 0)
            {
                await Task.Run(() => Encrypt(password));
                PasswordTxt.Text = "";
                MessageBox.Show("Operation complete");
            }
            else if(OperationCmb.SelectedItem.ToString().IndexOf("decrypt", StringComparison.CurrentCultureIgnoreCase) >= 0)
            {
                await Task.Run(() => Decrypt(password));
                PasswordTxt.Text = "";
                MessageBox.Show("Operation complete");
            }
        }

        private void Encrypt(string Password)
        {
            var keyFactory = new Rfc2898KeyFactory(Configuration.KeyLength, Configuration.SaltLength, Configuration.KeyIterations);
            var factory = new EncryptionFactory(keyFactory);
            factory.Encrypt(Password, FileTxt.Text);
        }

        private void Decrypt(string Password)
        {
            var keyFactory = new Rfc2898KeyFactory(Configuration.KeyLength, Configuration.SaltLength, Configuration.KeyIterations);
            var factory = new DecryptionFactory(keyFactory);
            factory.Decrypt(Password, FileTxt.Text);
        }
    }
}
