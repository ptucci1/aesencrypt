﻿namespace GUI
{
    public class Configuration
    {
        public int KeyLength { get; set; }
        public int SaltLength { get; set; }
        public int KeyIterations { get; set; }
    }
}
